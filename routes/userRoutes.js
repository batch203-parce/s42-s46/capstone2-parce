const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

console.log(userControllers);

// User registration
router.post("/register", userControllers.registerUser);

// User authentication
router.post("/login", userControllers.logInUser);

// Retrieve user details
router.get("/details", auth.verify, userControllers.retrieveUserDetails);

// Non-admin check-out
router.post("/checkout", auth.verify, userControllers.checkout);

module.exports = router;