const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

console.log(productControllers);

// Creating a new product (admin only)
router.post("/create", auth.verify, productControllers.createProduct);

// Retrieve all products (admin only)
router.get("/all", auth.verify, productControllers.retrieveAllProducts);

// Retrieve all active products
router.get("/active", productControllers.retrieveAllActive);

// Retrieve a single products 
router.get("/:productId", productControllers.retrieveSingleProduct);

// Update product information (admin only)
router.put("/:productId", auth.verify, productControllers.updateProduct);

// Archive product (admin only) 
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

// Order a product
router.get("/:productId", auth.verify, productControllers.getMyOrder);

module.exports = router;