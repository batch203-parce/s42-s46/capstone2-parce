const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth")


// User Registration
module.exports.registerUser = (req, res) => {
	console.log(req.body);

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);
	return newUser.save()
	.then(user => {
		console.log(user);
		res.send("Registered successfully");
	})
	.catch(err => {
		console.log(error);
		res.send(false);
	})
};

// User Authentication
module.exports.logInUser = (req, res) => {
	return User.findOne({email: req.body.email})
	.then(result => {
		if (result == null){
			return res.send({message: "No user found"});
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if (isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else {
				return res.send({message: "Incorrect Password!"});
			}
		}
	})
};


// Retrieve user details
module.exports.retrieveUserDetails = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	return User.findById(userData.id).then(result => {
		result.password = " ";
		res.send(result);
	})
};


// Create an order (User check-out)
module.exports.checkout = async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	// Admin is not allowed to order
	if(userData.isAdmin){
		return res.send("Not authorize to order");
	}

	// For user checkout
	else{
		let isUserUpdated = await User.findById(userData.id).then(user => {
		user.orders.push({
		totalAmount: req.body.totalAmount,
		products: req.body.products
		})

		return user.save()
		.then(result => {
			res.send("You ordered a product");
		})
		.catch(error => {
			console.log(error);
			res.send(error);
			})
		})

		for (let i=0; i < req.body.products.length; i++){

		let data = {
			userId: userData.id,
			email: userData.email,
			productId: req.body.products[i].productId,
			productName: req.body.products[i].productName,
			quantity: req.body.products[i].quantity
		}

		let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orders.push({
			userId: data.userId,
			email: data.email,
			quantity: req.body.quantity
		})

		// stocks - quantity
		product.stocks -= data.quantity;

		return product.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
				})
			})
		}

	}
}
