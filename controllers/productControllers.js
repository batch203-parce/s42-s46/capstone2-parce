const Product = require("../models/Product");
const auth = require("../auth");

// Creating product (admin only)
module.exports.createProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		stocks: req.body.stocks
	});

	if(userData.isAdmin){
		return newProduct.save()
		.then(product => {
			console.log(product);
			res.send("Product created successfully")
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		});
	}
	else {
		return res.send("You don't have access to this page!");
	};
};


// Retrieve all products (admin only)
module.exports.retrieveAllProducts = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else {
		return rest.status(401).send("You don't have access to this page!");
	}
};

// Retrieve all active products 
module.exports.retrieveAllActive = (req, res) => {
	return Product.find({isActive: true}).then(result => res.send(result));
};


// Retrieve a single products 
module.exports.retrieveSingleProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => res.send(result));
}


// Update product information (admin only)
module.exports.updateProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}
		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})	
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	}
	else{
		return res.status(401).send("You don't have access to this page!");
	}
}


// Archive product (admin only)
module.exports.archiveProduct = (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let updateActiveField = {
		isActive: req.body.isActive
	}

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(req.params.productId, updateActiveField, {new:true})
		.then(result =>{
			res.send("Archived successfully");
		})
		.catch(error => {
			console.log(error);
			console.log(false);
		})
	}
	else {
		return res.status(401).send("You don't have access to this page!");
	}
}	

// Order a product
module.exports.getMyOrder = (req, res) => {
	return Product.findById(req.params.productId).then(result => res.send(result));
}
